package com.domain;

import java.io.Serializable;

public class Homeworks implements Serializable{
    /**
	 * 
	 */
	public static final int Page_size=6;
	private static final long serialVersionUID = 1L;
	private int id;
    private String publisher;
    private String receiver;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublish(String publisher) {
		this.publisher = publisher;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
}
