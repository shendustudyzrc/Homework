package com.services;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import com.domain.Homeworks;
public interface IHomeworkService {
	//查询总的记录数
	public HashMap<String, String> selectCount()throws SQLException;
	//查找作业列表
	public List<Homeworks> select(HashMap<String,Object> map)throws SQLException;
	//教师增加作业
	public boolean saveHomeworks(Homeworks homeworks)throws SQLException;
	//教师更新作业
	public boolean updateHomeworks(Homeworks homeworks)throws SQLException;
	//教师根据id来删除作业信息
	public boolean delete(int id)throws SQLException;
	//教师根据id来查找作业信息
	public Homeworks selectById(int id)throws SQLException;
}
