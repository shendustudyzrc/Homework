package com.services.imp;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.stereotype.Service;
import com.domain.Homeworks;
import com.services.IHomeworkService;
@Service
public class HomeworksServiceImp implements IHomeworkService{
	 private static SqlSessionFactory sqlSessionFactory;
	    private static Reader reader;
	    static{
			  try {
				reader=Resources.getResourceAsReader("mybatis-config.xml");
				sqlSessionFactory=new SqlSessionFactoryBuilder().build(reader);
			} catch (IOException e) {
				e.printStackTrace();
			}  
		  }
	@Override
	public HashMap<String, String> selectCount() throws SQLException {
		HashMap<String,String> map=new LinkedHashMap<String,String>();
		SqlSession session=null;
		session=sqlSessionFactory.openSession();
		String a=String.valueOf(session.selectOne("com.domain.UserMapper.selectCountById"));
		map.put("a", a);
		return map;
	}
	@Override
	public List<Homeworks> select(HashMap<String, Object> map) throws SQLException {
		 SqlSession session=null;
		 session=sqlSessionFactory.openSession();
		 List<Homeworks> list=new LinkedList<Homeworks>();
		 list=session.selectList("com.domain.UserMapper.selectUserByMap",map);
		 return list;
	}
	@Override
	public boolean saveHomeworks(Homeworks homeworks) throws SQLException {
		boolean b=false;
		SqlSession session=null;
		session=sqlSessionFactory.openSession();
		int n=session.insert("com.domain.UserMapper.insertUserByAdmin", homeworks);
	    if(n>0){
	    	b=true;
	    }
	    session.commit();
	    return b;
	}
	@Override
	public boolean updateHomeworks(Homeworks homeworks) throws SQLException {
		boolean b=false;
		SqlSession session=null;
		session=sqlSessionFactory.openSession();
		int n=session.update("com.domain.UserMapper.updateUser",homeworks);
		if(n>0){
			b=true;
		}
		session.commit();
		return b;
	}
	@Override
	public boolean delete(int id) throws SQLException {
		boolean b=false;
		SqlSession session=null;
		session=sqlSessionFactory.openSession();
		int n=session.delete("com.domain.UserMapper.deleteUserById", id);
		if(n>0){
			b=true;
		}
		session.commit();
		return b;
	}
	@Override
	public Homeworks selectById(int id) throws SQLException {
		Homeworks homeworks=new Homeworks();
		SqlSession session=null;
		session=sqlSessionFactory.openSession();
		homeworks=session.selectOne("com.domain.UserMapper.selectOneById", id);
		session.commit();
		return homeworks;
	}
}
