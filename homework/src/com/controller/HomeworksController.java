package com.controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.domain.Homeworks;
import com.services.IHomeworkService;
@Controller
@RequestMapping(value="/student")
public class HomeworksController {
	@Autowired
	private IHomeworkService ius;
	  //查询用户信息列表
	  @RequestMapping(value="StudentInformationListjsp",method=RequestMethod.GET)
	  public void page(HttpServletRequest request,HttpServletResponse response) throws IOException{
		  System.out.println("进来了");
		  HttpSession session=request.getSession();
	      List<Homeworks> list=new LinkedList<Homeworks>();
	      //当前页码
		  int currpage=1;
		  //判断传递页码是否有效
		  if(request.getParameter("page")!=null){
		  //对当前页码赋值
			currpage=Integer.parseInt(request.getParameter("page"));
		  }
		  System.out.println(currpage);
		  HashMap<String,Object> map=new HashMap<String,Object>();
		  map.put("start",(currpage-1)*Homeworks.Page_size);
		  map.put("end", Homeworks.Page_size);
		  try {
			list=ius.select(map);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      session.setAttribute("list", list);
	      //总页数
		  int pages;
		  HashMap<String, String> map1=new HashMap<String,String>();
		  try {
			map1=ius.selectCount();
		  }catch (SQLException e) {
			e.printStackTrace();
		  }
		  int count=Integer.parseInt(map1.get("a"));
		  System.out.println(count);
		  //计算总页数
		  if(count%Homeworks.Page_size==0){
				//对总页数赋值
			    pages=count/Homeworks.Page_size;
		   }else{
				pages=count/Homeworks.Page_size+1;
			}
		  //实例化StringBuffer
			StringBuffer sb=new StringBuffer();
			for(int i=1;i<=pages;i++){
				//判断是否为当前页
				if(i==currpage){
					//构建分页条
					sb.append("["+i+"]");
				}else{
					//构建分页条
     			sb.append("<a href='http://localhost:9090/Homework/web/student/StudentInformationListjsp?page="+ i+ "'>"+i+"</a>");
		
				}
				//构建分页条
				sb.append(" ");
			}
		  session.setAttribute("bar", sb.toString());
	      response.sendRedirect("http://localhost:9090/Homework/HomeworkList.jsp");
   }
	  //管理员新增用户信息
	  @RequestMapping(value="/insertUser",method=RequestMethod.GET)
	  public void insert(HttpServletRequest request,HttpServletResponse response) throws IOException, SQLException{
		  String publisher=request.getParameter("publisher");
		  String receiver=request.getParameter("receiver");
		  Homeworks homeworks=new Homeworks();
		  homeworks.setPublish(publisher);
          homeworks.setReceiver(receiver);
          boolean b=ius.saveHomeworks(homeworks);
		  if(b==true){
			  response.sendRedirect("http://localhost:9090/Homework/web/student/StudentInformationListjsp");
		  }else{  
		  }
	  }
	  //管理员更新用户信息
	  @RequestMapping(value="/update",method=RequestMethod.GET)
	  public void update(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
		  int id=Integer.parseInt(request.getParameter("id"));
		  String publisher=request.getParameter("publisher");
		  String receiver=request.getParameter("receiver");
	    //String profession=request.getParameter("profession");
		  Homeworks homework=new Homeworks();
		  homework.setId(id);
		  homework.setPublish(publisher);
		  homework.setReceiver(receiver);
		  boolean b=ius.updateHomeworks(homework);
		  System.out.println(b);
		  if(b==true){
			  response.sendRedirect("http://localhost:9090/Homework/web/student/StudentInformationListjsp");
		  }else{  
			  response.sendRedirect("http://localhost:9090/HelloJavaWorld/error.jsp");
		  }
	 }
	  //管理员根据id删除用户信息
	  @RequestMapping(value="/deleteHw",method=RequestMethod.GET)
	  public void delete(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
		  request.setCharacterEncoding("UTF-8");
		  int id=Integer.parseInt(request.getParameter("id"));
		  boolean b=ius.delete(id);
		  if(b==true){
			  response.sendRedirect("http://localhost:9090/Homework/web/student/StudentInformationListjsp");
		  }else{  
			  response.sendRedirect("http://localhost:9090/HelloJavaWorld/error.jsp");
		  }
	 }
	  
	  //管理员根据id查找用户信息
	  @RequestMapping(value="/selectHwById",method=RequestMethod.GET)
	  public void select(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
		  request.setCharacterEncoding("UTF-8");
		  int id = 0;
		  if(request.getParameter("searchcontent")!=""){
		     id=Integer.parseInt(request.getParameter("searchcontent"));
		     Homeworks homeworks=ius.selectById(id);
			  HttpSession session=request.getSession();
			  List<Homeworks> list=new LinkedList<Homeworks>();
			  list.add(homeworks);
			  session.setAttribute("list", list);
			 /* response.sendRedirect("http://localhost:9090/Homework/HomeworkList.jsp");*/
		  }
	 }
}