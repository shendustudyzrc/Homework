<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.domain.Homeworks"%>
<%@ page import="java.util.List"%>
<%String contextPath=request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="js/jquery.min.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.json-2.2.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<title>教师界面</title>
<script type="text/javascript">
       function shanchu(id){
    	   console.log(id);
    	   confirm_=confirm("确定删除该列表？");
    	   if(confirm_){
    	   $.ajax({
			   url:"<%=contextPath%>/web/student/deleteHw?id="+id, 
		       type:'GET',
		       contentType:'application/json',
		       success:function(data){
		         alert("成功啦"); 
		         window.location.href="<%=contextPath%>/HomeworkList.jsp"
		       },
		       error:function(request){
		    	 alert("失败啦");  		    	   
		       }
		   });  
    	   }
    	 }
       function xiugai(id,publisher,receiver){
    	   $("#id").val(id);
    	   $("#publisher").val(publisher);
    	   $("#receiver").val(receiver);
    	   $("#myUpdateModal").modal('show');
    	   
       }
       function chaxun(){
    	   var searchcontent=$("#searchcontent").val();
    	   $.ajax({
			   url:"<%=contextPath%>/web/student/selectHwById?searchcontent="+searchcontent, 
		       type:'GET',
		       contentType:'application/json',
		       success:function(data){
		         alert("成功啦"); 
		         window.location.href="<%=contextPath%>/HomeworkList.jsp"
		       },
		       error:function(request){
		    	 alert("失败啦");  		    	   
		       }
		   }); 
       }
</script>
<style type="text/css">
* {
	margin: 0px;
	padding: 0px;
}

.head {
	width: 100%;
	height: 80px;
	background: #98bf76;
}

.body {
	top: 140px;
	height: 670px;
	width: 100%;
	background: #f6f6f6;
}

.footer {
	width: 100%;
	height: 100px;
	background: #98bf21;
}

.btn {
	margin-left: 60px;
	width: 250px;
	height: 30px;
	font-size: 15px;
	outline: none;
	border: 0;
	background: #98bf76;
}

.input {
	width: 250px;
	height: 40px;
}

.body-left {
	width: 10%;
	height: 650px;
	background: white;
	float: left;
}

.body-right {
	float: left;
}
</style>
</head>
<body>
	<div class="container1">
		<!-- 头部复用start -->
		<iframe src="header.jsp" style="width: 100%; height: 67px;"
			frameborder="0" scrolling="no"></iframe>
		<!-- 头部复用end -->
		<div class="body">
			<div class="body-left">
				<ul>
					<li style="font-size: 20px; padding-left: 20px" class="user"><p
							style="background: orange">
							<a href="<%=contextPath%>/web/student/StudentInformationListjsp">作业管理</a>
						</p></li>
				</ul>
			</div>
			<div class="body-right">
				 <div class="container">
					<h2>作业信息表格</h2>
			<form id="ff_sort_type_list" class="form-inline container-fluid search-form">
			  	<div id="searchDiv" class="row">
				  	<div class="form-input col-md-2">
				  		<div class="form-input-label">
					    	<label for="searchcontent" class="control-label"></label>
				  		</div>
				  		<div class="form-input-input">
					    	<input name="searchcontent" type="text" class="form-control" id="searchcontent" placeholder="请输入接收人">
				  		</div>
				  	</div>
				  	<div class="form-input col-md-6">
				  		<div class="form-input-input">
					    	<button style="width: 120px" class="btn btn-primary btn-lg" onclick="chaxun()">查询</button>
				  		</div>
				  	</div>
			  	</div>
			</form>
					<div style="padding-left: 960px;">
						<button style="width: 120px;" class="btn btn-primary btn-lg"
							data-toggle="modal" data-target="#myInsertModal">增加</button>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>作业ID</th>
									<th>发布者</th>
									<th>接收人</th>
									<th colspan="3">操作</th>
								</tr>
							</thead>
							 <div class="container">
								<div class="table-responsive">
								<%
                                 List<Homeworks> list=(List<Homeworks>)session.getAttribute("list");
                                  for(Homeworks h:list){
	                            %>
									<tr>
										<td><%=h.getId()%></td>
										<td><%=h.getPublisher()%></td>
										<td><%=h.getReceiver()%></td>
										<td style="width: 50px"><button style="width: 120px;"
												class="btn btn-primary btn-lg" onclick="xiugai('<%=h.getId()%>','<%=h.getPublisher()%>','<%=h.getReceiver()%>')">修改</button></td>
										<td style="width: 50px"><button style="width: 120px"
												class="btn btn-primary btn-lg"
												onclick="shanchu('<%=h.getId()%>')">删除</button></td>
									</tr>
									<%}%>
									<tr>
										<td align="center" colspan="11" bgcolor="white"><%=session.getAttribute("bar")%>
										</td>
									</tr>
								</div>
							</div> 
						</table>
					</div>
				</div> 
			</div>
		</div>
		<!-- 模态框（Modal） 增加表单-->
		<div class="modal fade" id="myInsertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">增加作业信息</h4>
					</div>
					<div class="modal-body">
						<!-- 添加的方法 -->
						<form class="form-horizontal" id="ajaxInsertForm"
							action="<%=contextPath%>/web/student/insertUser">
							<div class="form-group">
								<label for="lastname" class="col-sm-2 control-label">发布人</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="publisher"
										placeholder="请输入发布人姓名">
								</div>
							</div>
							<div class="form-group">
								<label for="lastname" class="col-sm-2 control-label">接收者</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="receiver"
										placeholder="请输入接受者">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-10"
									style="margin-left: 45px;">
									<input type="submit" style="width: 120px;" value="提交"
										class="btn btn-primary btn-lg">
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- //模态框更新的方法 -->
		<div class="modal fade" id="myUpdateModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">更新学生信息</h4>
					</div>
					<div class="modal-body">
						<!-- 更新的方法 -->
						<form class="form-horizontal" id="ajaxInsertForm"
							action="<%=contextPath%>/web/student/update">
							<div class="form-group">
								<label for="lastname" class="col-sm-2 control-label">id</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="id" id="id"
										 placeholder="请输入id">
								</div>
							</div>
							<div class="form-group">
								<label for="lastname" class="col-sm-2 control-label">发布人</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="publisher" id="publisher"
										 placeholder="请输入发布人姓名">
								</div>
							</div>
							<div class="form-group">
								<label for="lastname" class="col-sm-2 control-label">接收者</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="receiver" id="receiver"
										placeholder="请输入接受者">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-10"
									style="margin-left: 45px;">
									<input type="submit" style="width: 120px;" value="提交"
										class="btn btn-primary btn-lg">
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭
						</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal -->
		</div>
		<!-- 尾部复用start -->
		<iframe src="end.jsp" style="width: 100%; height: 90px;"
			frameborder="0" scrolling="no"></iframe>
		<!-- 尾部复用end-->
	</div>
</body>
</html>