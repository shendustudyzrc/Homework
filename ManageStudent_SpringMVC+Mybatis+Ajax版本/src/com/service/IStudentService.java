package com.service;
import java.util.HashMap;
import java.util.List;
import com.domain.Student;
public interface IStudentService {
	public boolean insertStudent(Student student);
    public boolean updateStudent(Student student);
    public Student selectStudentById(int id);
    public Student selectStudentByName(String name);
    public boolean deleteStudentById(int id);
    public List<Student> selectList();
    public HashMap<String, String> selectCount();
    public List<Student> selectStudentByMap(HashMap<String,Object> map);
}
