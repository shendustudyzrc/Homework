package com.service.imp;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.stereotype.Service;
import com.domain.Student;
import com.service.IStudentService;
@Service
public class StudentServiceImp implements IStudentService{
	  private static SqlSessionFactory sqlSessionFactory;
	  private static Reader reader;
	  static{
		  try {
			reader=Resources.getResourceAsReader("mybatis-config.xml");
			sqlSessionFactory=new SqlSessionFactoryBuilder().build(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}  
	  }
	@Override
	public boolean insertStudent(Student student) {
		  boolean b=false;
		  SqlSession session=null;
		  session=sqlSessionFactory.openSession();
		  int count=session.insert("com.domain.StudentMapper.insertStudent", student);
		  if(count==0){
			  b=false;
		  }else{
			  b=true;
		  }
		  session.commit();
		  return b;
	}
	@Override
	public boolean updateStudent(Student student) {
		  boolean b=false;	  
		  SqlSession session=null;
		  session=sqlSessionFactory.openSession();
	   	  int count=session.update("com.domain.StudentMapper.updateStudent", student);
		  if(count==0){
			  b=false;
		  }else{
			  b=true;
		  }
	   	  session.commit();
		  return b;
	}
	@Override
	public Student selectStudentById(int id) {
		 SqlSession session=null;
		  session=sqlSessionFactory.openSession();
		  Student student=session.selectOne("com.domain.StudentMapper.findStudentById", id);
		  if(student==null){
			 return null;
		  }else{
			  return student;
		  }
	}
	@Override
	public boolean deleteStudentById(int id) {
		 boolean b=false;
		 SqlSession session=null;
		  session=sqlSessionFactory.openSession();
		  int count=session.delete("com.domain.StudentMapper.deleteStudent", id);
		  if(count==0){
			  b=false;
		  }else{
			  b=true;
		  }
		  session.commit();
		  return b;
	}
	@Override
	public List<Student> selectList() {
		List<Student> list=new LinkedList<Student>();
		SqlSession session=sqlSessionFactory.openSession();
		list=session.selectList("com.domain.StudentMapper.selectListStudent");
		return list;
	}
	@Override
	public Student selectStudentByName(String name) {
		SqlSession session=sqlSessionFactory.openSession();
		Student student=new Student();
		student=session.selectOne("com.domain.StudentMapper.findStudentByName", name);
		return student;
	}
	@Override
	public HashMap<String, String> selectCount() {
	    SqlSession session=sqlSessionFactory.openSession();
	    HashMap<String, String> map=new HashMap<String, String>();
	    String a=String.valueOf(session.selectOne("com.domain.StudentMapper.Count"));
	    map.put("a", a);
	    return  map;
	}
	@Override
	public List<Student> selectStudentByMap(HashMap<String, Object> map) {
		List<Student> list=new LinkedList<Student>();
		SqlSession session=sqlSessionFactory.openSession();
	    list=session.selectList("com.domain.StudentMapper.selectStudentByMap", map);
		return list;
	}
}
