package com.controller;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.domain.Student;
import com.service.IStudentService;
@Controller
@RequestMapping(value = "/student")
public class StudentController {
	@Autowired
	private IStudentService istudentservice;
	//登录
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public void m001(HttpServletRequest request,HttpServletResponse response)throws SQLException,IOException{
		/*ModelAndView mav=new ModelAndView();*/
		request.setCharacterEncoding("UTF-8");
		Student student=new Student();
		String username=request.getParameter("username");
		student=istudentservice.selectStudentByName(username);
		if(student!=null){
			/*mav.setViewName("studentIndex");*/
			response.sendRedirect("studentIndex");
		}else{
			response.sendRedirect("http://localhost:9090/ManageStudent/studentIndex.jsp");
			/*mav.setViewName("add");*/
		}
		/*return mav;*/
	}
	
	
	@RequestMapping(value="/selectbyid",method=RequestMethod.GET)
	public ModelAndView m0(HttpServletRequest request,HttpServletResponse response)throws SQLException,IOException{
		ModelAndView mav=new ModelAndView();
		request.setCharacterEncoding("UTF-8");
		Student student=new Student();
		int id=Integer.parseInt(request.getParameter("id"));
		student=istudentservice.selectStudentById(id);
		HttpSession session=request.getSession();
		session.setAttribute("student", student);
		if(student!=null){
			mav.setViewName("showid");
		}else{
			mav.setViewName("add");
		}
		return mav;
	}
	/*@RequestMapping(value="/selectbyname",method=RequestMethod.GET)
	public ModelAndView m00(HttpServletRequest request,HttpServletResponse response)throws SQLException,IOException{
		ModelAndView mav=new ModelAndView();
		request.setCharacterEncoding("UTF-8");
		List<Student> list=new LinkedList<Student>();
		String name=request.getParameter("name");
		list=istudentservice.selectStudentByName(name);
		HttpSession session=request.getSession();
		session.setAttribute("list", list);
		if(list!=null){
			mav.setViewName("showname");
		}else{
			mav.setViewName("add");
		}
		return mav;
	}*/
	    @RequestMapping(value="/add",method=RequestMethod.GET)
	    public ModelAndView m1(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
	    	ModelAndView mav=new ModelAndView();
	    	request.setCharacterEncoding("UTF-8");
	    	Student student=new Student();
	    	String name=request.getParameter("name");
	    	String sex=request.getParameter("sex");
	    	Integer age=Integer.parseInt(request.getParameter("age"));
	    	String grade=request.getParameter("grade");
	    	String student_class=request.getParameter("student_class");
	    	String profession=request.getParameter("profession");
	    	student.setName(name);
	    	student.setGrade(grade);
	    	student.setProfession(profession);
	    	student.setSex(sex);
	    	student.setStudent_class(student_class);
	    	student.setAge(age);
			boolean b=istudentservice.insertStudent(student);
			HttpSession session=request.getSession();
			if(b==true){
				 List<Student> list=new LinkedList<Student>();
			     list=istudentservice.selectList();
			     session.setAttribute("list", list);
	        	 mav.setViewName("StudentInformationList");
			}else{
				mav.setViewName("add");
			}
			return mav;
	    }
	    @RequestMapping(value="/update",method=RequestMethod.GET)
	    public ModelAndView m2(HttpServletRequest request,HttpServletResponse response) throws SQLException, IOException{
	       ModelAndView mav=new ModelAndView();
	       request.setCharacterEncoding("UTF-8");
	   	   int id=Integer.parseInt(request.getParameter("id")); 
	   	   String name=request.getParameter("name");
	   	   String sex=request.getParameter("sex");
	   	   int age=Integer.parseInt(request.getParameter("age"));
	   	   String grade=request.getParameter("grade");
	   	   String student_class=request.getParameter("student_class");
	   	   String profession=request.getParameter("profession");
	   	   Student student=new Student();
	   	   student.setId(id);
	       student.setName(name);
    	   student.setGrade(grade);
    	   student.setProfession(profession);
    	   student.setSex(sex);
    	   student.setStudent_class(student_class);
    	   student.setAge(age);
	   	   boolean b=istudentservice.updateStudent(student);
	   	   HttpSession session=request.getSession();
	       if(b==true){
	    	   List<Student> list=new LinkedList<Student>();
			     list=istudentservice.selectList();
			     session.setAttribute("list", list);
	        	 mav.setViewName("StudentInformationList");
	       }else{
	    	   mav.setViewName("update");
	       }
	       return mav;
	    }
	    @RequestMapping(value="delete",method=RequestMethod.GET)
	    public ModelAndView delete(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
	    	ModelAndView mav=new ModelAndView();
	    	request.setCharacterEncoding("UTF-8");
	    	 int id=Integer.parseInt(request.getParameter("id"));
	         boolean b=istudentservice.deleteStudentById(id);
	         HttpSession session=request.getSession();
	         if(b==true){
	        	 List<Student> list=new LinkedList<Student>();
			     list=istudentservice.selectList();
			     session.setAttribute("list", list);
	        	 mav.setViewName("StudentInformationList");
	         }else{
	    	   mav.setViewName("mav");
	         }
	        return mav;
	    } 
	    @RequestMapping(value="insertjsp",method=RequestMethod.GET)
	    public ModelAndView insert(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
	    	ModelAndView mav=new ModelAndView();
	    	request.setCharacterEncoding("UTF-8");
			 mav.setViewName("add");
			return mav;
	    }
	    @RequestMapping(value="updatejsp",method=RequestMethod.GET)
	    public ModelAndView update(HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
	    	ModelAndView mav=new ModelAndView();
	    	request.setCharacterEncoding("UTF-8");
	    	mav.setViewName("update");
	        return mav;
	    }
	   @RequestMapping(value="StudentInformationListjsp",method=RequestMethod.GET)
	   public ModelAndView page(HttpServletRequest request,HttpServletResponse response){
		      ModelAndView mav=new ModelAndView();
		      HttpSession session=request.getSession();
		      List<Student> list=new LinkedList<Student>();
		      //当前页码
			  int currpage=1;
			  //判断传递页码是否有效
			  if(request.getParameter("page")!=null){
			  //对当前页码赋值
				currpage=Integer.parseInt(request.getParameter("page"));
			  }
			  System.out.println(currpage);
			  HashMap<String,Object> map=new HashMap<String,Object>();
			  map.put("start",(currpage-1)*Student.Page_size);
			  map.put("end", Student.Page_size);
			  list=istudentservice.selectStudentByMap(map);
		      session.setAttribute("list", list);
		      //总页数
			  int pages;
			  HashMap<String, String> map1=new HashMap<String,String>();
			  map1= istudentservice.selectCount();
			  int count=Integer.parseInt(map1.get("a"));
			  //计算总页数
			  if(count%Student.Page_size==0){
					//对总页数赋值
				    pages=count/Student.Page_size;
			   }else{
					pages=count/Student.Page_size+1;
				}
			  //实例化StringBuffer
				StringBuffer sb=new StringBuffer();
				for(int i=1;i<=pages;i++){
					//判断是否为当前页
					if(i==currpage){
						//构建分页条
						sb.append("["+i+"]");
					}else{
						//构建分页条
						sb.append("<a href='http://localhost:9091/ManageStudent/web/student/StudentInformationListjsp?page="+ i+ "'>"+i+"</a>");
					}
					//构建分页条
					sb.append(" ");
				}
			  session.setAttribute("bar", sb.toString());
		      mav.setViewName("StudentInformationList");
		      return mav;
	   }
	   
 }

