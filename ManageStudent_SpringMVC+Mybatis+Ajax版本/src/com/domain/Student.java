package com.domain;
public class Student {
  public static final int Page_size=2;
  private Integer id;
  private String name;
  private String sex;
  private Integer age;
  private String grade;
  private String student_class;
  private String profession;
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSex() {
	return sex;
}
public void setSex(String sex) {
	this.sex = sex;
}
public Integer getAge() {
	return age;
}
public void setAge(Integer age) {
	this.age = age;
}
public String getGrade() {
	return grade;
}
public void setGrade(String grade) {
	this.grade = grade;
}
public String getStudent_class() {
	return student_class;
}
public void setStudent_class(String student_class) {
	this.student_class = student_class;
}
public String getProfession() {
	return profession;
}
public void setProfession(String profession) {
	this.profession = profession;
}
@Override
public String toString() {
	return "Student [id=" + id + ", name=" + name + ", sex=" + sex + ", age=" + age + ", grade=" + grade
			+ ", student_class=" + student_class + ", profession=" + profession + "]";
}
  
}
