package com.domain;

public class PageBean {
  //开始数
  private Integer start;
  //结束数
  private Integer end;
  public PageBean(){	
  
  }
  public PageBean(Integer start,Integer end){
	 /* super();*/
	  this.start=start;
	  this.end=end;
  }
  public Integer getStart() {
	return start;
  }
  public void setStart(Integer start) {
	this.start = start;
  }
  public Integer getEnd() {
	return end;
  }
  public void setEnd(Integer end) {
	this.end = end;
  }
}
