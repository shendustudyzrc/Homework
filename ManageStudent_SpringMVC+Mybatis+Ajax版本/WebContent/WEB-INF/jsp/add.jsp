<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
  String contextPath=request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Bootstrap的输入框代码</title>
<!--     <link href="http://cdn.bootcss.com/bootstrap/3.3.1/css/bootstrap-theme.min.css" rel="external nofollow" rel="external nofollow"
        rel="stylesheet">
    <link href="http://cdn.bootcss.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="external nofollow" rel="external nofollow"
        rel="stylesheet"> -->
    <script type="text/javascript" src="http://localhost:9090/ManageStudent/js/jquery.min.js"></script>
<!--     <script src="http://cdn.bootcss.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.1/js/npm.js"></script> -->
    <script type="text/javascript">
       function insert(){
    		   $.ajax({
                 /*  url:"http://localhost:9090/ManageStudent/ajax.jsp", */
    			   url:"/ManageStudent/web/student/add",
    		       type:'GET',
    		       data:$('#ajaxForm').serialize(),
    		       async:false,
    		       contentType:'application/json;charset=UTF-8',
    		       success:function(data){
    		    	  alert("成功啦");
    		       },
    		       error:function(request){
    		    	 alert("失败啦");   		    	   
    		       }
    		   });
    	   }
    </script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
    <a href="/ManageStudent/ajax.jsp">ajax.jsp</a>
   
        <div class="container-fluid">
            <!-- 导航图标 -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#" rel="external nofollow" rel="external nofollow" rel="external nofollow" rel="external nofollow">
                    <img alt="Brand" src="http://localhost:9090/ManageStudent/images/reg.gif">
                </a>
            </div>
            <form class="navbar" id="ajaxForm" name="form1">
                <div class="form-group">
                    <input type="text" class="form-control" name="id" placeholder="学号">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="姓名">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="age" placeholder="年龄">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="sex" placeholder="性别">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="grade" placeholder="年级">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="student_class" placeholder="班级">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="profession" placeholder="专业">
                </div>
                <input type="submit" class="insert" onClick="insert()" value="调用一下Ajax"/>
            </form>
             <div id="ajaxDiv"></div>
        </div>
    </nav>
</body>
</html>