<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.domain.Student"%>
<%@ page import="java.util.List"%>
<%
   String contextPath=request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生信息展示页面</title>
<script>
 function check(form1){
	  var ids=document.getElementByName("id");
	  for(var i=0;i<ids.length;i++){
		  if(ids[i].checked){
			  return true;
		  }
	  }
	  alert("请选择你要删除的学生信息");
	  return false;
  }
</script>
</head>
<body>
<form action="<%=contextPath%>/web/student/selectbyid" method="get">
<table>
   <tr>
   <td>根据学号id查询信息：</td>
   <td>
   <input type="text" name="id" id="id">
   <input type="submit" name="submit" value="提交">
   </td>
  </tr>
</table>
</form>
<form action="<%=contextPath%>/web/student/selectbyname" method="get">
<table>
    <tr>
   <td>根据学生姓名查询信息：</td>
   <td>
   <input type="text" name="name" id="name">
   <input type="submit" name="submit" value="提交">
   </td>
  </tr>
</table>
</form>
<div><button>添加</button></div>
<div><button onclick="javascript:window.location.reload()">刷新页面</button></div>
<div><button onclick="javascript:history.go(-1)">返回上一级界面</button></div>
<form name="form1" action="DeleteStudentBatchServlet" method="post" onsubmit="return check(this)">
 <table border="1" width="200">
   <tr><td colspan="9"><img src="/ManageStudent/WebContent/images/图书馆管理系统.jpg"></td></tr> 
   <tr>
   <td width=20><input type="submit" value="批量删除"></td>
   <td>学号</td>
   <td>姓名</td>
   <td>性别</td>
   <td>年龄</td>
   <td>年级</td>
   <td>班级</td>
   <td>专业</td>
   <td colspan="3">操作</td>
   </tr>
 <%
   List<Student> list=(List<Student>)session.getAttribute("list");
   for(Student student:list){
	   %>
   <tr>
   <td><input type="checkbox" name="id" value=<%=student.getId()%>></td>
   <td><%=student.getId()%></td>
   <td><%=student.getName()%></td>
   <td><%=student.getSex()%></td>
   <td><%=student.getAge()%></td>
   <td><%=student.getGrade()%></td>
   <td><%=student.getStudent_class()%></td>
   <td><%=student.getProfession()%></td>
   <td><a href="<%=contextPath%>/web/student/insertjsp">增加</a></td>
   <td><a href="<%=contextPath%>/web/student/updatejsp">修改</a></td>
   <td><a href="<%=contextPath%>/web/student/delete?id=<%=student.getId()%>">删除</a></td>
   </tr>
   <%
   }
   %> 
   <tr>
		<td align="center" colspan="11" bgcolor="white">
			<%=session.getAttribute("bar")%>
		</td>
   </tr> 
 </table>
</form>
</body>
</html>