<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.domain.Student" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>信息展示</title>
</head>
<body>
<table border="1" width="600px" height="600px">
<tr><td colspan="7">学生信息</td></tr>
<hr/>
 <tr>
   <td>学号</td>
   <td>姓名</td>
   <td>性别</td>
   <td>年龄</td>
   <td>年级</td>
   <td>班级</td>
   <td>专业</td>
   </tr>
<%
   List<Student> list=(List<Student>)session.getAttribute("list"); 
    for(Student student:list){
%>
<tr><td><%=student.getId()%></td>
<td><%=student.getName()%></td>
<td><%=student.getSex()%></td>
<td><%=student.getAge()%></td>
<td><%=student.getGrade()%></td>
<td><%=student.getStudent_class()%></td>
<td><%=student.getProfession()%></td><tr>
<% 
}%>
</table>
</body>
</html>