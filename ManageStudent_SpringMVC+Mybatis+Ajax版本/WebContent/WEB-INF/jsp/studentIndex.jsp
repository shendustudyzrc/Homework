<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
  String contextPath=request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>学生管理系统界面</title>
        <meta http-equiv="content-Type" content="text/html;charset=utf-8" />
        <!--Bootstrap CSS-->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
        <style type="text/css">
           .container{
            width: 100%;
            height: 400px;
            border-color: "green";
            background: gainsboro;
           }
           .navbar-header{
               width: 100%;
               height: 50px;
               color: "red";
               border: red;
               background:gray;
           }
           .shouye{
               background: green;
               left: 20%;
               margin-left: 20px;
               left: 100px;
           }
           .person{
               background: greenyellow;
           }
           .class{
               background: red;
           }
           .grade{
               background: gray;
           }
           .navbar-left{
               background: gold;
               float: left;
               height: 900px;
               width: 237px;
           }
           .navbar-right{
               background: green;
               height: 900px;
           }
        </style>
        <script type="text/javascript">
          var seconds=1;
          var URL;
          function Load(){
        	  for(var i=seconds;i>=0;i--){
        		  windows.setTimeout('doUpdate('+i+')',(seconds-i)*1000); 
        	  }
        	/*   alert("aa"); */
          }
          function doUpdate(num){
        	  document.getElementById('ShowDiv').innerHTML="将在"+num+"秒后跳转主页";
              if(num==0){
            	  window.location.href="www.baidu.com";
              }
          } 
        </script>
        <script type="text/javascript">
        function check(){  
          alert("aa");
        }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <table border="1" width=100% height=50px><tr><td class="shouye" width="20%">首页</td><td class="person" width=20%>个人信息</td><td class="class" width="20%">班级信息</td><td class="grade" width="20%">年级信息</td><td class="select" align="right" width="20%">查询&nbsp;<input type="text" name="select" placeholder="可输入学号\姓名\班级\年级"></td></tr></table>
                </div>  
                <div class="navbar-leftright">
                    <div class="navbar-left">
                    <ul class="nav navbar-nav">
                        <li><input type="button" value="学生信息管理系统" onclick="javaScript:window.location.href='<%=contextPath%>/web/student/StudentInformationListjsp'"/></li>
                        <li><input type="button" value="关闭" onClick="javascript:window.close();"/></li>
                        <li><a href="#" onClick="javaScript:window.location.reload();window.close()">返回</a></li>
                        <li><button onclick="javaScript:history.go(-1)">去下一级</button>
                        <li><button onclick="Load()">跳转页面</button></li>
                        <li><div id="ShowDiv"></div></li>
                        <li><button onClick="return check();">ss</button></li>
                    </ul>
                    </div>
                     <div class="navbar-right">  
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>